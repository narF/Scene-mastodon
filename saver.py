# -*- coding: utf-8 -*-
"""
Everything needed to save and load from savefiles

Created on Thu Nov  2 17:15:00 2023
@author: narF
"""
import jsonpickle

def save(filename: str, data: any) -> None:
    print(f"Saver: saving to {filename}")
    encoded_players = jsonpickle.encode(data, indent=3)
    with open(filename, 'w', encoding='utf-8') as file:
        file.write(encoded_players)


def load(filename: str) -> any:
    print(f"Saver: load({filename})")
    with open(filename, 'r', encoding='utf-8') as file:
        return jsonpickle.decode(file.read())
