#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 23:16:09 2023

@author: narF
"""

from player import Player
from director import Director
import saver
from settings import debug as debug_flag



class Bouncer():
    '''Bouncer is the Player Manager. In charge of maintaining the list of players and saving and loading it from a file. Also in charge of fixing errors in Players'''
    
    # _player_list = {}
    
    def __init__(self, players_filename: str, director: Director):
        self.director = director
        self._players_filename = players_filename
        self._player_list = {}
        # self.load(players_filename)
        print(f"Bouncer __init__: {self} :: {self._player_list}")
    
    
    def register_new_player(self, player_id: str) -> Player :
        if not self.player_is_registered(player_id):
            starting_name = self.director.DEFAULT_PLAYER_NAME
            starting_location = self.director.DEFAULT_PLAYER_LOCATION
            new_body = self.director.create_body(starting_location, starting_name)
            
            player = Player(new_body, self.director)
            self._player_list[player_id] = player
            
            return player
        else:
            assert False # Trying to register a player who's already registered. Is it bad?
            player = self.get_player(player_id)
        
        self.verify_and_fix_player_location(player)
        return player
        
    
    def player_is_registered(self, player_id: str) -> bool:
        return player_id in self._player_list
    
    
    def get_player(self, player_id: str) -> Player:
        if not self.player_is_registered(player_id):
            self.register_new_player(player_id)
        
        player = self._player_list[player_id]
        # self.verify_and_fix_player_Node_exists(player)
        # self.verify_and_fix_player_where(player)
        return player
    
    
    def load(self, file: str) -> None:
        try:
            self._player_list = saver.load(file)
            
        except FileNotFoundError as err:
            if debug_flag:
                print(f"WARNING: Bouncer savefile {file} is missing.")
                print(err)
                self._player_list = {}
            else:
                raise err
                
        except Exception as err:
            if debug_flag:
                print(f"WARNING: Bouncer couldn't load the savefile {file} for some reason.")
                print(err)
            else:
                raise err
        
        
    def save(self) -> None:
        print("Bouncer.save()")
        saver.save(self._players_filename, self._player_list)
        
        
    # def verify_and_fix_player_Node_exists(self, player: Player) -> None:
    #     '''Verify if the player's Body exists. If not, attempts to fix it.'''
    #     try:
    #         assert type(player.body) == Player
    #     except Exception:
    #         # player's body was deleted, so we reset to default
    #         print(f"WARNING: Bouncer: Player id {player.body} doesn't exist anymore. Reverting to {DEFAULT_PLAYER_ID}")
    #         # TODO: Warn the player that his Body disappeared
    #         player.body = DEFAULT_PLAYER_ID
        
        
    def verify_and_fix_player_where(self, player: Player) -> None:
        '''Verify if the player's location on record matches with the real location of its body. If not, attempts to fix it automatically.'''
        
        if player.where != player.body.parent:
            print(f"WARNING: Bouncer: location mismatch. Supposed location: {player.where} Real location:{player.body.parent}")
            # player.where = player.body.parent
            print(f"Fixed location: {player}")
            self.save()

    def __str__(self) -> str:
        return f"Bouncer {id(self)} {self.director}"