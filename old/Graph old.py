from dataclasses import dataclass, field
from typing import Any, List


class Test:
    pass

@dataclass
class Node:
    content: Any
    id: int
    links: List[Any] = field(default_factory=lambda:[]) #le parent c'est toujours la première
    
    def setParent(self, parent):
        child = self
        if (child not in parent.links):
            parent.links.append(child)
            
        if (parent in child.links):
            child.links.remove(parent)
            
        if len(child.links) >0 :
            child.links[0] = parent
        else:
            child.links.append(parent)
    
    def getParent(self):
        return self.links[0]

@dataclass
class Graph:
    initialContent: Any
    nextId = 0
    head: Node = None
    
    def __post_init__(self):
        self.head = Node(self.initialContent, self.nextId)
        self.nextId += 1
        self.head.links.append(self.head)
        self.initialContent = None
    
    def addNode(self, parent, content):
        newNode = Node(content, self.nextId)
        self.nextId += 1
        parent.links.append(newNode)
        return newNode
    
    def getNode(self, id):
        assert(fail)
    
    def setNode(self, id, content):
        assert(fail)
        
    def __iter__(self):
        traversed = []
        queue = []
        queue.append(self.head) #commence par ajouter la tête
            
        while len(queue) != 0: #passe au travers de la queue
            node = queue[0]
            queue = queue[1:]
            if node not in traversed:
                traversed.append(node)
                for child in node.links:
                    queue.append(child)
            

        return iter(traversed)
        

monde = Graph("monde") #nouveau graph
bb2 = monde.addNode(monde.head, "amérique")
monde.addNode(bb2, "canada")
bb3 = monde.addNode(bb2, "québec")
monde.addNode(bb2, "Ontario")
monde.addNode(bb3, "montréal")
monde.addNode(bb3, "Laval")
monde.addNode(bb3, "Repentigny")
monde.addNode(monde.head, "Europe")
monde.addNode(monde.head, "Asie")
monde.addNode(monde.head, "Australie")
usa = monde.addNode(bb3, "USA")
usa.setParent(bb2)
print(len(usa.links), usa.links[0].content)


# print(f"Monde: {monde}")

# for node in monde:
#     print (node.id, node.content)
#     pass


