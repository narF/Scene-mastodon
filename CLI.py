import director
from commander import Commander

class CLI:
    def __init__(self, player_id:str):
        self.player_id = player_id
        self.commander = Commander()
        print(f"Init {self}")
    
    def __repr__(self):
        return f"CLI: player_id={self.player_id}"


    def readInput(self):
        try: 
            return input("?> ")
        except KeyboardInterrupt:
            director.saveAndQuit()
        except EOFError:
            director.saveAndQuit()

    def loop(self):
        while True:
            raw_command = self.readInput()
            result = self.commander.executeRawCommand(self.player_id, raw_command)
            print(result)
            

if __name__ == "__main__":
    c = CLI("fake admin narF")
    
    def fakeCommand(txt):
        print(f"?> {txt}")
        print(c.commander.executeRawCommand(c.player_id, txt))
        
    fakeCommand("describe")
    fakeCommand("become")
    fakeCommand("become e")
    fakeCommand("become asdf")
    fakeCommand("create narF")
    fakeCommand("become narF")
    fakeCommand("enter bébé")
    fakeCommand("create prout")
    # fakeCommand("leave")
    # fakeCommand("leave")
    # fakeCommand("leave")
    fakeCommand("debug")
    fakeCommand("")
    
    c.loop()