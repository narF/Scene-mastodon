"""
The Director is in charge of managing the Stage.

Created on Tue Oct 23 22h59 2022

@author: narF
"""

from enum import Enum
from typing import List

from tree import Tree
from Body import Body
import saver


SearchMode = Enum("SearchMode", ["SIBLINGS", "PARENT", "CHILDREN"])
SAVEFILE = "savefile.json"

DEFAULT_PLAYER_NAME = "bébé"

class Director:
    '''The Director is in charge of managing the scene.'''
    def __init__(self):
        self.scene = Tree(Body("Scène", ""))
        self.DEFAULT_PLAYER_LOCATION = self.scene.head
        self.DEFAULT_PLAYER_NAME = DEFAULT_PLAYER_NAME


    def create_body(self, location: Body, new_body_name: str, description: str = "") -> Body:
        if self.destination_is_valid(location, name = new_body_name):
            new_body = Body(new_body_name, description)
            location.linkChild(new_body)
            return new_body
        else:
            raise BodyConflictError(f"There's already a body {new_body_name} in {location}.", target_name=new_body_name, destination=location)
                
    
    def destroy_body(self, body: Body) -> None:
        if body != self.scene.head:
            body.delete()
        else:
            raise ValueError("The Scene cannot be destroyed!")
    
    
    def move(self, moving_body: Body, destination: Body) -> None:
        '''Move `moving_body` inside `destination`, so that it's parent is now `destination`.
        
        Raises:
            BodyConflictError: if `destination` contains a body with the same name as `moving_body`.
        '''
        if moving_body != self.scene.head:
            if self.destination_is_valid(destination, moving_body=moving_body):
                moving_body.move(destination)
            else:
                raise BodyConflictError(f"ERROR! Trying to move {moving_body.name} into {destination.name} but destination already has a Body named {moving_body.name}: {destination.children}", target_name = moving_body.name, destination = destination)
        else:
            raise ValueError("The Scene cannot be moved!")
    
    
    # def change_name(self, target, newName) -> None:
    #     # change name of target
    #     self.scene.getNode(target).content.name = newName
    
    
    # def changeDescription(self, target, newText) -> None:
    #     '''Change description of target Body. Should be deprecated maybe?'''
    #     self.scene.getNode(target).content.description = newText
    
    
    # def change_parent_description(self, target, newText) -> None: 
    #     """Change description of target's parent because that's what you actually do in the game."""
    #     self.scene.getNode(target).parent.content.description = newText
    
    
    # def locateLocalPerfect(self, location: Body, txt: str) -> List[Body]:
    #     """Search in children"""
        
    #     children = scene.getChildren(location)
        
    #     perfectMatches = [child for child in children if txt.lower() == child.content.name.lower()]
        
    #     assert len(perfectMatches) <= 1
    #     return perfectMatches
    
    
    # def locateLocalFuzzy(self, location: Body, txt: str) -> List[Body]:
    #     """Search in children"""
        
    #     children = scene.getChildren(location)
    #     return [child for child in children if txt.lower() in child.content.name.lower()]
    
    
    # def locateSibling(self, target: Body, name:str, allowFuzzy: bool = False) -> Body:
    #     """
    #     Locate a sibling body of target that matches the name. If a perfect match can't be found, try to find a body with a name that contains name.
    
    #     Parameters
    #     ----------
    #     target : Body or nodeId(int)
    #         Will look for siblings of this Body.
    #     name : str
    #         The search query.
    #     allowFuzzy : bool, optional
    #         Should probably be renamed "partialSearch"🤔 If True, will also look for names that contains the search query. The default is False.
    
    #     Returns
    #     -------
    #     Body
    #         The id(int) of the Body matching the name (or containing the name if allowFuzzy is True)
    #     """
        
    #     target = scene.getNode(target)
    #     located = self.locateLocalPerfect(target.parent, name)
    #     if len(located) == 1:
    #         return located[0]
        
    #     if allowFuzzy:
    #         located = self.locateLocalFuzzy(target.parent, name)
    #         if len(located) == 1:
    #             return located[0]
    #     #     else:
    #     #         return None
    #     # else:
    #     #     return None
        
    def search(self,
               name: str, 
               target: Body | int, 
               *filters: int, 
               allowFuzzy: bool = False) -> Body:
        """
        Search for a Body named "name" around target that matches the provided filters.
    
        Args:
            - name: The name (or partial name if allowFuzzy) of a body to search for around the target.
            - target: The Body (or body id: int) to search in/around.
            - *filters: 1 or more filters SearchMode.SIBLINGS, .PARENT or .CHILDREN
            - allowFuzzy: DESCRIPTION. Defaults to False.
    
        Raises:
            - NotFoundError: If no results
            - MultipleSearchResults: If more than 1 result
        """
        lookIn = set()
        
        if SearchMode.CHILDREN in filters:
            lookIn.update(target.children) # ".update()" adds the new items to the set
        if SearchMode.PARENT in filters:
            lookIn.update([target.parent])
        if SearchMode.SIBLINGS in filters:
            lookIn.update(target.siblings)
        
        results = {body for body in lookIn if name.lower() == body.name.lower()}
    
        # if exact match, return this exact match
        if len(results) == 1:
            return results.pop() # returns the first and only body in the set
        
        if allowFuzzy:
            results = {body for body in lookIn if name.lower() in body.name.lower()}
            if len(results) == 1:
                return results.pop() # returns the first and only body in the set
            
        if len(results) == 0:
            raise NotFoundError(f"No results searching \"{name.lower()}\" in {target=}.")
        else:
            raise MultipleSearchResults(f"Multiple results searching \"{name.lower()}\" in {target=} : {results}", results = results)
        
    
    def destination_is_valid(self, destination: Body, *, name: str = None, moving_body: Body = None) -> bool:
        """
        Ensure that the destination doesn't already contain a Body with the same name. Either name or movingNode must be provided.
    
        Raises
        ------
        ValueError: Raised if both name and movingNode aren't provided.
    
        """
        assert destination != moving_body
        
        if moving_body != None:
            assert destination not in moving_body.children
        
        if moving_body != None:
            name = moving_body.name
            
        if name != None:
            names_in_destination = [body.name.lower() for body in destination.children]
            return name.lower() not in names_in_destination
        
        raise ValueError(f"Missing arguments. Either name or moving_body must be provided. Received {name=} {moving_body=}.")
    
    
    def describe(self, who: Body) -> str:
        """Returns a text description of who you are, where you are, what's around you and what's inside you."""
        if who.parent is not None:
            parentName = who.parent.name
        else:
            parentName = "le NÉANT"
        
        blabla = f"Tu es {who.name} dans {parentName}."
        
        if (who.parent is not None) and (who.parent.description != ""):
            blabla += f"\n\nDescription: {who.parent.description}"
    
        # Énumérer siblings (Bodies autours)
        if len(who.siblings) > 0:
            blabla += f"\n\nTu vois: {pretty_list(who.siblings)}."
    
        # Énumérer contenu
        if len(who.children) > 0:
            blabla += f"\n\nTu contiens: {pretty_list(who.children)}."
    
        return blabla
    
    
    # def toJson() -> str:
    #     import jsonpickle
    #     return jsonpickle.encode(scene, indent=2)
    
    
    # def importFromJson(json_txt: str) -> Graph:
    #     import jsonpickle
    #     return jsonpickle.decode(json_txt)
        
        
    def save(self, filename: str = SAVEFILE) -> None:
        saver.save(filename, self.scene)
    
    
    def load(self, filename: str = SAVEFILE, debug=False) -> None:
        try:
            self.scene = saver.load(filename)
                
        except FileNotFoundError as e:
            if (debug):
                print(f"WARNING! Scene database \"{filename}\" not found. Will ignore since we're in debug.")
            else:
                raise e
                
        except ValueError as e:
            if (debug):
                print("WARNING! Scene database is not valid json. Will ignore this since we're in debug.")
            else:
                raise e



def pretty_list(body_list: List[Body], conjonction: str = "et") -> str:
    '''Retourne une phrase à partir d\'une liste de Bodies. Ex: "patate, tomate et poivron"'''
    
    bla = ""
    for index, body in enumerate(body_list):
        if index == 0:
            bla += body.name
        else:
            if index != len(body_list)-1:  # pas le dernier
                bla += f", {body.name}"
            else:
                bla += f" {conjonction} {body.name}"
    return bla


class BodyConflictError(Exception):
    def __init__(self, *args, target_name: str, destination: Body):
        super().__init__(*args)
        self.target_name = target_name
        self.destination = destination


class NotFoundError(Exception):
    def __init__(self, *args):
        super().__init__(*args)


class MultipleSearchResults(Exception):
    def __init__(self, *args, results: List[Body] = []):
        super().__init__(*args)
        self.results = results


if __name__ == "__main__":
    d = Director()
    
    print(d.describe(d.scene.head))
