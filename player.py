# -*- coding: utf-8 -*-
"""
@author: narf
"""
from dataclasses import dataclass

from director import Director
from Body import Body

@dataclass
class Player:
    body: Body #la node que le joueur contrôle
    # _where: Body  #la node où se trouve le joueur. Utile en cas qu'un joueur B a effacé le Body du joueur A.
    director: Director #the Director who controls the scene in which this Player is located

    @property    
    def where(self):
        return self.body.parent
    
    @property
    def who(self) -> Body:
        print("WARNING: Player.who is deprecated. Use Player.body instead.")
        return self.body
    
    @who.setter
    def who(self, new_body) -> None:
        print("WARNING: Player.who is deprecated. Use Player.body instead.")
        self.body = new_body
