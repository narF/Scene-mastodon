"""
@author: narF
"""
from director import Director
from bouncer import Bouncer
from parser import UserCommand


# Import each commands
from Commands.Create import Create_Command
from Commands.Destroy import Destroy_Command
from Commands.Describe import Describe_Command
from Commands.Become import Become_Command
from Commands.Move import Move_Command
from Commands.Leave import Leave_Command
from Commands.Take import Take_Command
from Commands.Drop import Drop_Command
from Commands.Save import Save_Command
from Commands.Load import Load_Command
from Commands.Debug import Debug_Command


PLAYERS_FILENAME = "saved_players.json"

class Commander:
    """The Commander receives commands and sends instructions to the Director and the Bouncer."""
    
    registeredCommands = [Create_Command(), 
                          Destroy_Command(), 
                          Describe_Command(), 
                          Become_Command(), 
                          Move_Command(), 
                          Leave_Command(), 
                          Take_Command(), 
                          Drop_Command(), 
                          Save_Command(), 
                          Load_Command(),
                          Debug_Command()]
    
    
    def __init__(self):
        self.director = Director()
        self.bouncer = Bouncer(PLAYERS_FILENAME, self.director)
    
    
    def register(self, command):
        self.registeredCommands.append(command)
    
    
    def executeRawCommand(self, playerID: str, rawCommand: str) -> str:
        player = self.bouncer.get_player(playerID)
        userCommand = UserCommand(player, rawCommand)
        director = player.director
        
        if userCommand.verb == None:
            return director.describe(player.body)
        
        for command in self.registeredCommands:
            if userCommand.verb.lower() in command.verbs: # if the command can accept the verb used
                if command.verifyTarget(userCommand):
                    result = command.run(userCommand)
                    return result
                else:
                    return command._targetNotFound(userCommand)
        
        return f"**{userCommand.verb}??? Je comprends pas...**\n\n{director.describe(player.body)}"


if __name__ == "__main__":
    commander = Commander()
    
    print(commander.executeRawCommand("fake player", "go in europe"))
