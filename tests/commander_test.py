# -*- coding: utf-8 -*-
"""
Created on Wed Jan 17 21:29:59 2024

@author: narf
"""
import pytest

from commander import Commander
from bouncer import Bouncer
from director import Director
from player import Player

@pytest.fixture
def commander():
    return Commander()

@pytest.fixture
def player_id() -> str:
    return "pytest fake player"

@pytest.fixture
def player(commander, player_id) -> Player:
    return commander.bouncer.get_player(player_id)

@pytest.fixture
def scenario1(commander, player_id):
    run_these = ["create Amérique", "enter Amérique", "create USA", "create Canada", "create Mexique", "enter canada", "create Québec", "create Ontario", "enter Québec", "create Montréal", "create Laval", "create Repentigny", "enter Repentigny", "create maison", "leave", "leave", "leave", "leave", "leave", "debug"]
    for command in run_these:
        print(f"\n?> {command}")
        result = commander.executeRawCommand(player_id, command)
        print(result)
    return commander

def run_command_list(commander: Commander, player_id, *command_list: list) -> str:
    for command in command_list:
        print()
        print(f"?> {command}")
        print( commander.executeRawCommand(player_id, command) )


def test_commander_sub_modules_type(scenario1, player_id):
    assert type(scenario1.bouncer) == Bouncer
    assert type(scenario1.director) == Director

def test_scenario1(scenario1: Commander, player_id):
    player = scenario1.bouncer.get_player(player_id)
    head = scenario1.director.scene.head 
    assert head == player.body.parent
    
    run_command_list(scenario1, player_id, "enter amérique", "enter USA")
    assert player.body.parent.name == "USA"
    
    
def test_create(scenario1: Commander, player_id, player):
    run_command_list(scenario1, player_id, "enter amérique", "enter Canada", "create toto")
    assert player.body.parent.name == "Canada"

    run_command_list(scenario1, player_id, "enter toto")
    assert player.body.parent.name == "toto"
    
    run_command_list(scenario1, player_id, "leave")
    assert player.body.parent.name == "Canada"
    
    run_command_list(scenario1, player_id, "enter tot")
    assert player.body.parent.name == "toto"
    
def test_create_already_existing(scenario1: Commander, player_id, player):
    run_command_list(scenario1, player_id, "enter amérique")
    out = scenario1.executeRawCommand(player_id, "create Canada") 
    assert out.find("**There's already a body with the name \"Canada\".**") == 0


def test_become(scenario1: Commander, player_id, player):
    run_command_list(scenario1, player_id, "become Amérique")
    assert player.body.name == "Amérique"
    assert player.body.parent.name == "Scène"
    
    run_command_list(scenario1, player_id, "become bébé", "enter Amérique", "become Canada")
    assert player.body.name == "Canada"
    assert player.body.parent.name == "Amérique"

def test_become2(scenario1, player_id, player):
    run_command_list(scenario1, player_id, "enter Amérique", "become Canada", "enter USA")
    assert player.body.name == "Canada"
    assert player.body.parent.name == "USA"
    assert "Québec" in [child.name for child in player.body.children]

def test_become_inexistant(scenario1: Commander, player_id, player):
    out = scenario1.executeRawCommand(player_id, "become asdf")
    assert out.find("There's no Body with the name \"asdf\".") == 0

def test_become_multiple_results(scenario1: Commander, player_id, player):
    run_command_list(scenario1, player_id, "enter amérique", "create can")
    out = scenario1.executeRawCommand(player_id, "become ca")
    assert out.find("I'm confused: \"ca\" could mean:") == 0

def test_take(scenario1, player_id, player):
    out = scenario1.executeRawCommand(player_id, "take")
    assert out.find("What do you want to take?") == 0
    
    out = scenario1.executeRawCommand(player_id, "take asdf")
    assert out.find("Could not find anything named \"asdf\".") == 0
    
    out = scenario1.executeRawCommand(player_id, "take amé")
    assert out.find("You took Amérique.") == 0
    assert "Amérique" in [child.name for child in player.body.children]
    
    scenario1.executeRawCommand(player_id, "create Amérique")
    out = scenario1.executeRawCommand(player_id, "take Amérique")
    assert out.find("Cannot take Amérique because you already contain something with this name.") == 0
    
def test_take_multiple_results(scenario1, player_id, player):
    scenario1.executeRawCommand(player_id, "create améri")
    out = scenario1.executeRawCommand(player_id, "take am")
    assert out.find("I'm confused. \"am\" could mean:") == 0

def test_no_crash(scenario1, player_id, player):
    run_command_list(scenario1, player_id, "asdf", "create bidule", "take", "take bidule", "drop", "drop bidule", "take bidu", "drop bidu", "create bidul", "take bid", "delete", "delete bidul", "take bid", "create bidule", "take bidule", "drop bidule", "destroy bi")
    
def test_leave_scene(scenario1, player_id, player):
    out = scenario1.executeRawCommand(player_id, "leave")
    assert out.find("You can't leave the main stage.\n\n") == 0

def test_leave_blocked_by_same_name(commander, player_id, player):
    run_command_list(commander, player_id, "create toto", "enter toto", "create toto", "become toto")
    out = commander.executeRawCommand(player_id, "leave")
    assert out.find("Can't leave this room because there's a body over there that has the same name as you: \"toto\". \n\n") == 0
    
