# -*- coding: utf-8 -*-
import pytest
from tree import Node, Tree

@pytest.fixture
def baby(): return Node(uid="baby")

@pytest.fixture 
def child1(baby): 
    c1 = Node(uid="child1")
    c1.linkChild(baby)
    return c1

@pytest.fixture 
def child2(): return Node(uid="child2")

@pytest.fixture
def parent(child1, child2): 
    p = Node(uid="p")
    p.linkChild(child1)
    p.linkChild(child2)
    return p
    

def test_Node(child1, child2, parent):
    n = Node()
    n2 = Node()
    assert n != n2
    
    assert id(child1.children) != id(child2.children)
    
    # I shouldn't have to test this, but since I don't know much about id(), let's test it anyway!
    assert id(n) != id(n2)
    assert id(parent) != id(n2)
    assert id(parent) != id(child1)
    assert id(child1) != id(child2)
    

def test_linkChild(parent, child1, child2, baby):
    assert child1 in parent.children
    assert child2 in parent.children
    assert baby in child1.children
    assert baby not in parent.children
    assert baby not in child2.children
    
    assert child1._parent == parent
    assert child2._parent == parent
    assert baby._parent == child1
    
    with pytest.raises(Exception):
        child1.linkChild(baby)
    
    with pytest.raises(Exception):
        child1.linkChild(child2)
    

def test_siblings(parent, child1, child2, baby):
    assert parent.siblings == []
    assert baby.siblings == []
    assert child2.siblings == [child1]
    assert child1.siblings == [child2]
    
def test_removeChild(parent, child1, child2, baby):
    parent.removeChild(child1)
    assert child1 not in parent.children
    assert child2 in parent.children
    assert child1 not in child2.siblings
    assert child2 not in child1.siblings
    assert child1._parent is None
    assert baby in child1.children

def test_delete(parent, child1, child2, baby):
    parent.delete()
    assert child1._parent == None
    assert child2._parent == None
    assert parent.children == []
    
    baby.delete()
    assert child1.children == []
    assert child2.children == []
    
def test_delete2(parent, child1, child2, baby):
    child1.delete()
    assert parent.children == [child2]
    assert baby._parent == None
    assert child2.siblings == []

def test_drawChildren(parent, child1, child2, baby):
    bb = Node("bb")
    child2.linkChild(bb)
    parent.linkChild(Node("c3"))
    result = """p
|  ┞child1
|  |  ┞baby
|  ┞child2
|  |  ┞bb
|  ┞c3"""
    assert parent.drawChildren() == result
    
def test_Tree(parent, child1, child2, baby):
    t = Tree(parent)
    assert t.head == parent
    assert t.head.parent == None

def test_Tree2(parent, child1, child2, baby):
    t = Tree(child1)
    assert t.head == child1
    assert parent.children == [child2]
    assert child1.parent == None
    assert t.head.parent == None
    
    print(t)

def test_Tree3():
    t = Tree()
    assert type(t.head) == Node


# if __name__ == "__main__":
#     TestNode().test_Graph3()
