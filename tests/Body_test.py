# -*- coding: utf-8 -*-
import pytest
from Body import Body



def test_Body():
    b = Body("toto")
    assert b.name == "toto"
    assert b.description == ""
    assert b.parent is None
    
    b = Body("toto", "desc")
    assert b.description == "desc"
    
    with pytest.raises(AttributeError):
        print(b._id)
    


if __name__ == "__main__":
    ...