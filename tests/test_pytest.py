import pytest

from director import Director

    
@pytest.fixture
def director():
    return Director()
    
def test_director1():
    director.toto = 3
    
def test_director2(director):
    assert not hasattr(director, "toto")
