# -*- coding: utf-8 -*-
import pytest

import director as director_module

@pytest.fixture
def director():
    return director_module.Director()

@pytest.fixture
def grand_parent(director):
    return director.create_body(director.scene.head, "grand-parent")

@pytest.fixture
def parent(director, grand_parent):
    return director.create_body(grand_parent, "parent")

@pytest.fixture
def child(director, parent):
    return director.create_body(parent, "children")

@pytest.fixture
def baby(director, child):
    return director.create_body(child, "baby")

@pytest.fixture
def sister(director, parent):
    return director.create_body(parent, "sister")

@pytest.fixture
def aunt(director, grand_parent):
    return director.create_body(grand_parent, "aunt")


def test_Director(director):
    d1 = director_module.Director()
    d2 = director 
    
    assert id(d1) != id(d2)
    assert id(d1.scene) != id(d2.scene)

def test_create_body(director, parent, grand_parent, aunt, child):
    assert parent in grand_parent.children
    assert aunt in grand_parent.children
    assert child in parent.children
    
    assert grand_parent.parent == director.scene.head
    assert parent.parent == grand_parent
    assert aunt.parent == grand_parent
    assert child.parent == parent
    
def test_destroy_body(director, child, parent, sister, baby):
    director.destroy_body(child)
    
    assert child not in parent.children
    assert sister in parent.children
    assert baby.parent == None

def test_destroy_body2(director, sister, child, parent):
    director.destroy_body(sister)
    
    assert sister not in parent.children
    assert child in parent.children
    
    assert sister.children == []

def test_destroy_head(director, grand_parent):
    head = director.scene.head
    
    with pytest.raises(ValueError):
        director.destroy_body(head)
    
    assert grand_parent.parent == head
    assert grand_parent in head.children
    
def test_move_body(director, baby, sister, parent):
    director.move(baby, sister)
    
    assert baby.parent == sister
    assert baby in sister.children
    
    assert sister.parent == parent
    assert sister in parent.children

def test_move_body2(director, child, parent, aunt):
    director.move(child, aunt)
    
    assert child.parent == aunt
    assert child in aunt.children
    
    assert child not in parent.children
    assert child.parent != parent
    
def test_move_head(director, baby, grand_parent):
    head = director.scene.head
    
    with pytest.raises(ValueError):
        director.move(head, baby)
    
    assert head.parent == None
    assert head not in baby.children
    
    assert grand_parent.parent == head
    assert grand_parent in head.children

def test_destination_is_valid(director, grand_parent, parent, child):
    assert director.destination_is_valid(parent, moving_body=child) == False
    
    with pytest.raises(AssertionError):
        director.destination_is_valid(parent, moving_body=grand_parent)
    with pytest.raises(AssertionError):
        director.destination_is_valid(parent, moving_body=parent)
    with pytest.raises(ValueError):
        director.destination_is_valid(parent, name=None, moving_body=None) # Should fail because both name and moving_body are provided
    
def test_describe(director, parent, child):
    # only to verify it doesn't crash
    print(director.describe(parent))
    print(director.describe(child))
    # TODO: actual verification of the content
    
def test_pretty_list(director):
    ... # TODO
def test_search(director):
    ... # TODO
