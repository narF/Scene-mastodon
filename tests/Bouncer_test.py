# -*- coding: utf-8 -*-
"""
Tests for pytest for Bouncer
@author: narf
"""
import pytest

from bouncer import Bouncer
from director import Director
from player import Player

@pytest.fixture()
def director(): return Director()

@pytest.fixture()
def bouncer(director): return Bouncer("test_saved_players.json", director)


def test_Bouncer(director):
    b = Bouncer("test_saved_players.json", director)
    assert not b.player_is_registered("test")
    
def test_registering_players(bouncer):
    bouncer.register_new_player("test")
    assert bouncer.player_is_registered("test")

def test_registering_players_2(bouncer):
    print(f"test_registering_players_2:{bouncer} :: {bouncer._player_list}")
    
    # This should not be true because we use a pytest fixture
    # If this test fails, it's because there's a problem with the fixture
    assert not bouncer.player_is_registered("test")

def test_get_player(bouncer):
    print(f"test_get_player: {bouncer} :: {bouncer._player_list}")
    p = bouncer.get_player("test")
    print(p)
    assert type(p) == Player
