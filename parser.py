# from dataclasses import dataclass
from typing import List
from player import Player

uselessEng = ["the", "in", "a", "to"]
uselessFr = ["le", "les", "des", "du", "la", "un", "une", "ce", "ces"]

class UserCommand:
    '''
    Properties:
        - verb: str
        - target: str
        - player: Player
    '''
    
    def __init__(self, player: Player, input: str|List[str]):
        self.player = player
        
        if type(input) == str:
            parsedInput = _parse(input)
        elif type(input) == List:
            parsedInput = input
        else:
            raise ValueError(f"Expected type str or list. Received{type(input)}: {input}")
            
        if len(parsedInput) > 0 and parsedInput[0] != "":
            self.verb = parsedInput[0]
        else:
            self.verb = None
            assert len(parsedInput) == 1 # si c'est plus grand que 1 mais que le verbe est quand même vide, y'a un problème dans parse()
            
        if len(parsedInput) > 1:
            self.target = parsedInput[1]
        else:
            self.target = None
            
    def __str__(self):
        return f"UserCommand: {self.verb}, '{self.target}' from Player({self.player.body.name}, {self.player.body.parent.name})"


def _parse(textString: str) -> List[str]:
    textString = _cleanFirstWords(textString)
    command = textString.split(' ', 1)
    if len(command) > 1:
        command[1] = _cleanFirstWords(command[1])
        if command[1] == "":
            command.pop(1)
    return command

# def parseAsCommand(textString: str) -> UserCommand:
#     return UserCommand(parse(textString))

def _cleanFirstItems(list): # TODO this function seems unused...
    while list[0] in uselessEng+uselessFr:
        list = list.pop(0)
    return list


def _cleanFirstWords(txt: str) -> str:
    '''Enlève les déterminants and les mots inutiles mais seulement au début'''
    while True:
        split = txt.split(' ', 1)
        if split[0] in uselessEng+uselessFr:
            if len(split) > 1:
                txt = split[1] #flush the first word
            else:
                return ""
        else:
            return txt
    # ret = ""
    # for word in txt.split():
    #     if list[0] not in pronouns or list[0] not in déterminants:
    #         ret += word
    # return ret


if __name__ == "__main__":
    print(_parse(""))
    print(_parse("le allo"))
    print(_parse("le allo le"))
    print(_parse("le allo le chaise"))
    print(_parse("a in allo in le chaise pour le prout"))
    
    def fakeCommand(string): # This is just for debugging
        p = Player(1, 0)
        return UserCommand(p, string)
    
    print(fakeCommand(""))
    print(fakeCommand("le allo"))
    print(fakeCommand("le allo le"))
    print(fakeCommand("le allo le chaise"))
    print(fakeCommand("a in allo in le chaise pour le prout"))
