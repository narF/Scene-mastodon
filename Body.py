from typing import Self
from node import Node

class Body(Node):
    """Body:
    Player-created objects that go on the Scene controlled by the Director."""
    def __init__(self, name: str, description: str = "") -> Self:
        super().__init__()
        self.name = name
        self.description = description
        del self._id
    
    def __repr__(self) -> str:
        txt = f"<Body {self.name}>"
        
        if self.description != "":
            txt = txt + " " + self.description
            
        return txt
    
    def drawChildren(self, indentation: int = 0) -> str:
        out = f"{self.name} {str(id(self))[-4:]}"
        indentation += 1
        for child in self.children:
            out += f"\n{indentation*'|  '}┞{child.drawChildren(indentation)}"
        return out
    

if __name__ == "__main__":
    b = Body("toto")
    print(b)