#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 21:44:26 2023

@author: narf
"""
from Commands.Command import Command
from parser import UserCommand
import director


class Load_Command(Command):
    minimum_nb_words = 1
    verbs = ["load"]
    
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand):
        Director.load()
        Director.bouncer.load()
        return "Loaded savefile!" + "\n\n" + userCommand.player.describe()