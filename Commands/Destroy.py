# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 23:40:35 2023
@author: narF
"""

from Commands.Command import Command
from parser import UserCommand
import director as director_module
from director import SearchMode, NotFoundError, MultipleSearchResults

class Destroy_Command(Command):
    minimum_nb_words = 2
    verbs = ["destroy", "remove", "delete", "del", "rm"]
    
    def __init__(self):
        pass
        
    def run(self, userCommand: UserCommand) -> str:
        targetTxt = userCommand.target
        p = userCommand.player
        director = p.director
        
        try:
            target = director.search(targetTxt, p.body, SearchMode.SIBLINGS)
            director.destroy_body(target)
            return f"{target.name} was deleted.\n\n{director.describe(p.body)}"
        
        except NotFoundError:
            return f"There is no \"{targetTxt}\" to destroy.\n\n{director.describe(p.body)}"
        
        except MultipleSearchResults as e:
            return f"I'm confused: \"{targetTxt}\" could mean: {director_module.prettyList(e.results, 'ou')}.\n\n" + director.describe(p.body)
                
    
    def _targetNotFound(self, userCommand: UserCommand) -> str:
        p = userCommand.player
        director = p.director
        return f"What do you want to destroy? \n\n{director.describe(p.body)}"
