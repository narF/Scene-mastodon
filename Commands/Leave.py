# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 21:23:56 2023

@author: narf
"""
from Commands.Command import Command
from parser import UserCommand
from director import BodyConflictError

class Leave_Command(Command):
    minimum_nb_words = 1
    verbs = ["leave", "exit"]
    ignored_words= []
    
    def __init__(self):
        pass
        
        
    def run(self, userCommand: UserCommand):
        p = userCommand.player
        parent = userCommand.player.body.parent
        grandParent = parent.parent
        director = p.director
        
        if parent != director.scene.head:
            try:
                director.move(p.body, grandParent)
                # p.where = grandParent
                return f"You exited the {parent.name}. \n\n{director.describe(p.body)}"
            
            except BodyConflictError as conflict:
                return f"Can't leave this room because there's a body over there that has the same name as you: \"{conflict.target_name}\". \n\n" + director.describe(p.body)
            
        else:
            return "You can't leave the main stage.\n\n" + director.describe(p.body)
        