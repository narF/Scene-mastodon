from Commands.Command import Command
from parser import UserCommand

class Debug_Command(Command):
    minimum_nb_words = 1
    verbs=["debug"]
    ignored_words = [""] # TODO: Utiliser une constante partagée?
    
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand):
        print(userCommand.player)
        
        print(f"Node player: {userCommand.player.body.name}")
        print(f"Node location: {userCommand.player.body.parent.name}")
        return userCommand.player.director.scene.head.drawChildren()
