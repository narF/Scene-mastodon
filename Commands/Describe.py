#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 27 17:27:45 2023

@author: narf
"""
from Commands.Command import Command
from parser import UserCommand
# import Director

class Describe_Command(Command):
    minimum_nb_words=1
    verbs=["describe", "", "look"]
    
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand) -> str:
        player = userCommand.player
        director = player.director
        return director.describe(player.body)
