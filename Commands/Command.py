#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 23:54:16 2023

@author: narF
"""
from parser import UserCommand


class Command:
    '''The parent class of all Commands used by Commander.'''
    def __init__(self):
        raise NotImplementedError
    
    def verifyTarget(self, userCommand: UserCommand) -> None:
        if self.minimum_nb_words == 1:
            return True
        elif self.minimum_nb_words > 1 and userCommand.target is not None:
            return True
        else:
            return False
    
    # These methods are "abstract methods", aka placeholders which are meant to be replaced by the real method in each Command (sub-classes). 
    # They are here to show the intention of what they are supposed to look like
    # TODO : Replace this with the actual Python way of making abstract classes?
    def run(self, userCommand: UserCommand):
        raise NotImplementedError(f"Tried to call {self.verbs[0]}.run() but this command haven't implemented it.", userCommand)
    
    def _targetNotFound(self, userCommand: UserCommand):
        raise NotImplementedError(userCommand)