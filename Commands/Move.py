# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 23:40:35 2023
@author: narf
"""

from Commands.Command import Command
from parser import UserCommand
import director as director_module

class Move_Command(Command):
    minimum_nb_words = 2
    verbs=["move", "enter", "go"]
    ignored_words = ["dans"] # TODO: Utiliser une constante partagée?
    
    def __init__(self):
        pass

    
    def run(self, userCommand: UserCommand) -> str:
        destination = userCommand.target
        player = userCommand.player
        director = player.director
        
        try:
            destination = director.search(destination, player.body, director_module.SearchMode.SIBLINGS, allowFuzzy=True)
            director.move(player.body, destination)
            # player.where = destination
            return "Moved! \n\n" + director.describe(player.body)
        
        except director_module.NotFoundError:
            return f"There's no \"{destination}\" to go to.\n\n" + director.describe(player.body)
        except director_module.MultipleSearchResults as e:
            name_list = director_module.pretty_list(e.results, 'or')
            return f"I'm confused: \"{destination}\" could mean either {name_list}.\n\n" + director.describe(player.body)
        except director_module.BodyConflictError as conflict:
            return f"You can't move there because there's already a Body named \"{conflict.targetName}\"."
        
            
    def _targetNotFound(self, userCommand: UserCommand) -> str:
        player = userCommand.player
        director = player.director
        return f"Where do you want to go?\n\n{director.describe(player.body)}"
    