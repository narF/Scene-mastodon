#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 27 15:48:18 2023

@author: narf
"""
from Commands.Command import Command
from parser import UserCommand
import director as director_module
from director import Director

class Take_Command(Command):
    minimum_nb_words = 2
    verbs=["take", "get", "obtain", "collect"]
    ignored_words = [""] # TODO: Utiliser une constante partagée?
    
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand) -> str:
        target = userCommand.target
        p = userCommand.player
        director = p.director
        
        try: 
            target = director.search(target, p.body, director_module.SearchMode.SIBLINGS, allowFuzzy=True)
            director.move(target, p.body)
            return f"You took {target.name}.\n\n" + director.describe(p.body)
        except director_module.NotFoundError:
            return f"Could not find anything named \"{userCommand.target}\".\n\n" + director.describe(p.body)
        except director_module.MultipleSearchResults as err:
            return f"I'm confused. \"{userCommand.target}\" could mean: {director_module.pretty_list(err.results, 'ou')}. \n\n" + director.describe(p.body)
        except director_module.BodyConflictError:
            return f"Cannot take {target.name} because you already contain something with this name.\n\n" + director.describe(p.body)
            
    def _targetNotFound(self, userCommand: UserCommand) -> str:
        player = userCommand.player
        director = player.director
        return f"What do you want to take? \n\n{director.describe(player.body)}"
