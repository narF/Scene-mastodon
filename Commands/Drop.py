#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 21:40:54 2023

@author: narf
"""
from Commands.Command import Command
from parser import UserCommand
import director as director_module

class Drop_Command(Command):
    minimum_nb_words = 2
    verbs = ["drop", "place", "deposit", "put"]
    ignored_words = []
                     
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand):
        targetTxt = userCommand.target
        p = userCommand.player
        director = p.director
        
        try: 
            target = director.search(targetTxt, p.body, director_module.SearchMode.CHILDREN, allowFuzzy=True)
            director.move(moving_body = target, destination = p.body.parent)
            return f"You dropped {target.name}.\n\n" + director.describe(p.body)
        
        except director_module.MultipleSearchResults as e:
            return f"I'm confused: \"{targetTxt}\" could mean: {director_module.pretty_list(e.results, 'ou')}.\n\n" + director.describe(p.body)
        except director_module.NotFoundError:
            return f"There's no Body with the name \"{targetTxt}\".\n\n" + director.describe(p.body)
        except director_module.BodyConflictError:
            return f"You cannot drop {target.name} because there's already another object with that name there.\n\n" + director.describe(p.body)
    
    def _targetNotFound(self, userCommand: UserCommand):
        p = userCommand.player
        director = p.director
        return f"What do you want to drop? \n\n{director.describe(p.body)}"