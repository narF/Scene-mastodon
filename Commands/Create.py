# -*- coding: utf-8 -*-
"""
Created on Thu Oct 26 23:40:35 2023
@author: narF
"""
from Commands.Command import Command
from parser import UserCommand
import director as director_module

class Create_Command(Command):
    minimum_nb_words = 2
    verbs = ["create"]
    ignored_words = [""] # TODO: Utiliser une constante partagée?
    
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand) -> str:
        p = userCommand.player
        director = userCommand.player.director
        try: 
            director.create_body(p.body.parent, new_body_name = userCommand.target)
            return director.describe(p.body)
        except director_module.BodyConflictError:
            return f"**There's already a body with the name \"{userCommand.target}\".** \n\n" + director.describe(p.body)
    
    def _targetNotFound(self, userCommand: UserCommand) -> str:
        describe = userCommand.player.director.describe(userCommand.player.body)
        return f"What do you want to create??\n\n{describe}"
