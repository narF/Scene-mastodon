#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 21:42:40 2023

@author: narf
"""
from Commands.Command import Command
from parser import UserCommand
import director


class Save_Command(Command):
    minimum_nb_words = 1
    verbs = ["save"]
    
    def __init__(self):
        pass
    
    def run(self, userCommand: UserCommand):
            Director.save()
            Director.bouncer.save()
            return "Saved!" + "\n\n" + userCommand.player.describe()
