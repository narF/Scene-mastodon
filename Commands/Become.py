#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 28 21:48:21 2023

@author: narf
"""

from Commands.Command import Command
from parser import UserCommand
import director as director_module

class Become_Command(Command):
    minimum_nb_words = 2
    verbs=["become"]
    ignored_words = [""] # TODO: Utiliser une constante partagée?
    
    def __init__(self):
        ...
    
    def run(self, userCommand: UserCommand):
        
        target = userCommand.target
        p = userCommand.player
        director = userCommand.player.director
        
        try:
            target = director.search(target, p.body, director_module.SearchMode.SIBLINGS, allowFuzzy=True)
            p.body = target
            # p.where = target.parent
            return director.describe(p.body)
        except director_module.NotFoundError:
            return f"There's no Body with the name \"{userCommand.target}\".\n\n" + director.describe(p.body)
        except director_module.MultipleSearchResults as err:
            return f"I'm confused: \"{target}\" could mean: {director_module.pretty_list(err.results, 'ou')}.\n\n" + director.describe(p.body) 
        
            
    def _targetNotFound(self, userCommand: UserCommand) -> str:
        director = userCommand.player.director
        playerBody = userCommand.player.body
        return f"Who do you want to become?\n\n{director.describe(playerBody)}"
