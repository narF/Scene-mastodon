from typing import Self, List

class Node:
    """A Node is (usually) part of a Graph. 
    
    Limitation:
        - A node can only have 1 parent"""
    
    def __init__(self, uid: int = None) -> None:
        self._id = uid
        self._parent = None
        self.children = []
    
    # def __key(self): #Est-ce que c'est nécessaire?
    #     return (self._id, self._parent)
    
    
    # def __hash__(self): #Est-ce que c'est nécessaire?
    #     return hash(self.__key())

    
    def linkChild(self, newChild: Self) -> None:
        if newChild._parent is None:
            self.children.append(newChild)
            newChild._parent = self
        else:
            raise Exception("There's already a parent!")
        
        
    def removeChild(self, target: Self) -> None:
        '''Unlinks the target from self. The target must be a child of self.'''
        if target in self.children:
            self.children.remove(target)
            target._parent = None
        else:
            raise ValueError(f"Trying to remove a child {target} which isn't actually a child of {self}. Actual childrens are:{self.children}")
    
    
    def move(self, newParent) -> Self:
        self.parent.removeChild(self)
        newParent.linkChild(self)
        return self
    
    
    @property
    def siblings(self) -> List[Self]:
        if self.parent is not None:
            return [child for child in self.parent.children if child != self]
        else:
            return []
    
    
    def delete(self) -> None:
        if self.parent is not None:
            self.parent.removeChild(self)
            
        children = list(self.children) # make a copy of the list, to ensure that the for-loop will iterate and delete each of them. Without this, the for-loop skips items.
        for child in children:
            self.removeChild(child)
    
    
    def __repr__(self):
        if self._id is not None:
            return f"Node id={self._id}"
        else:
            return f"Node id={id(self)}"
    
        
    def drawChildren(self, indentation: int = 0) -> str:
        if hasattr(self, "_id"):
            out = f"{self._id}"
        else:
            out = f"{id(self)}"
        indentation += 1
        for child in self.children:
            out += f"\n{indentation*'|  '}┞{child.drawChildren(indentation)}"
        return out
    
    
    @property
    def parent(self) -> Self:
        return self._parent
