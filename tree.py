# -*- coding: utf-8 -*-
"""
Created on Fri Jan  5 19:15:35 2024

@author: narf
"""
from typing import Self

from node import Node

class Tree:
    def __init__(self, startingNode: Node = None) -> Self:
        if startingNode is None:
            startingNode = Node()
            
        self.head = startingNode
        
        # The head should not have a parent. So if it does, we get rid of it.
        if startingNode.parent is not None:
            startingNode.parent.removeChild(startingNode)
            startingNode._parent = None

    # def __contains__():
    #     raise"not implemented yet"
        
    # def __iter__():
    #     raise "not implemented yet"
    
    def __repr__(self) -> str:
        return self.head.drawChildren()
