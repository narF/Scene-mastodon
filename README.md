# Scene-mastodon

Mastodon bot that put objects on a scene

Very WIP!!

## What currently work
- The CLI module
- The commands to create, destroy, take, drop, become, enter, leave, describe

## What currently doesn't work
- Persistance: saving and loading a `Scene` to a file on disk
- reading from or posting to Mastodon (early exploration work in branch `mastodon-module`)

## Overview
- A `Scene` is a tree of `node`
- `Director`: In charge of managing the Scene
- `Bouncer`: Manages players and rate-limit
- `Commander`: Receives commands, parse them, validates them with the `Bouncer` and sends them to the `Director`

## Setup dev environment
(You certainly don't need all this. But that's how I like to work. Dependancies are listed in `environment.yml`. You can install them using your method of choice.)
1. Install [miniconda](https://docs.conda.io/projects/miniconda/en/latest/miniconda-install.html) 
2. Create the environment from the file: `conda env create -f environment.yml` 
3. Open the environment: `conda activate scene-mastodon`
4. [Install Spyder](https://docs.spyder-ide.org/current/installation.html#installing-with-conda)
   1. Create an environment for spyder: `conda create -c conda-forge -n spyder-env spyder` (this will also installs Spyder in the environment)
   2. Go to the environment: `conda activate spyder-env`
   3. Recommended for more reliable updates: `conda config --env --add channels conda-forge && conda config --env --set channel_priority stric`
   4. Launch spyder: `spyder`
5. Setup Spyder
   1. Tools > Preferences > Python interpreter
   2. Check "Use the following Python interpreter"
   3. Select the one with `scene-mastodon` in its path
   4. Relaunch the kernel

## Contribution
I generally don't know what I'm doing, and I certainly don't know when I do something wrong! 😅 If you're a real programmer who actually know how to code, feel free to suggest improvement if you see that something is messy. 